package com.example.oappoc;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.oappoc.domain.EnhancedComment;
import com.example.oappoc.domain.EnhancedPost;
import com.example.oappoc.domain.Post;
import com.example.oappoc.domain.SimpleComment;
import com.example.oappoc.domain.SimplePost;
import com.example.oappoc.repository.PostRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DemoApplicationTests {
	/** The logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DemoApplicationTests.class);

	@Autowired
	private PostRepository<Post> postRepository;

	@Autowired
	private PostRepository<EnhancedPost> enhancedPostRepository;

	@Autowired
	private PostRepository<SimplePost> simplePostRepository;

	@Test
	@Rollback(false)
	@Transactional
	public void contextLoads() {

		/*
		 * Create and persist Simple comment & post.
		 */
		createSimplePost();

		/*
		 * Create and persist Enhanced comment & post.
		 */
		createEnhancedPost();

		/*
		 * Get all kinds of posts.
		 */
//		getPosts();
	}

	private void getPosts() {

		final List<Post> posts = postRepository.findAll();

		final List<EnhancedPost> enhancedPosts = enhancedPostRepository.findAll();

		final List<SimplePost> simplePosts = simplePostRepository.findAll();

//		LOGGER.info(String.format("%s%s%s", "\u001B[31m", enhancedPosts.get(0).getComments().get(0).getText(), "\u001B[0m"));

		LOGGER.info(String.format("%s%s%s", "\u001B[35m", "----- End of getPosts()", "\u001B[0m"));
	}

	private void createSimplePost() {

		SimpleComment simpleComment = new SimpleComment();
		simpleComment.setText("This is a simple comment.");

		SimplePost simplePost = new SimplePost();

		// we prefer to add rather set a comment - see comment in addComment implementation
//        simplePost.setComments(Collections.singletonList(simpleComment));
		simplePost.addComment(simpleComment);

		simplePostRepository.save(simplePost);
	}

	private void createEnhancedPost() {

		EnhancedComment enhancedComment1 = new EnhancedComment();
		enhancedComment1.setText("This is an enhanced comment.");
		enhancedComment1.setCreatedAt(LocalDateTime.now());

		EnhancedComment enhancedComment2 = new EnhancedComment();
		enhancedComment2.setText("This is another one enhanced comment.");
		enhancedComment2.setCreatedAt(LocalDateTime.now());

		EnhancedPost enhancedPost = new EnhancedPost();

		// we prefer to add rather set a comment - see comment in addComment implementation
//        enhancedPost.setComments(Collections.singletonList(enhancedComment1));
		enhancedPost.addComment(enhancedComment1);
        enhancedPost.addComment(enhancedComment2);

		enhancedPost.setImportant(true);
		enhancedPost.setReadOnly(false);
		enhancedPost.setStartDate(LocalDateTime.now());

		final EnhancedPost ep = enhancedPostRepository.save(enhancedPost);

//		ep.setReadOnly(true);
//		ep.setImportant(false);
		ep.getCommentDetails().getComments().get(0).setText("TESTING");
//		ep.getCommentDetails().getComments().get(1).setText("TESTING2");
//		ep.getCommentDetails().setEndDate(LocalDateTime.now().plusDays(200));
//		ep.getCommentDetails().setLastUpdated(LocalDateTime.now().plusDays(100));
//
		LOGGER.info(String.format("%s%s%s", "\u001B[35m", "----- End of createEnhancedPost()", "\u001B[0m"));
	}
}