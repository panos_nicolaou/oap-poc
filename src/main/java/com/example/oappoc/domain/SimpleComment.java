package com.example.oappoc.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class SimpleComment extends Comment<SimplePost> {
}