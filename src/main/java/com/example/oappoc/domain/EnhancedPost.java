package com.example.oappoc.domain;

import java.time.LocalDateTime;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class EnhancedPost extends Post<EnhancedCommentDetails> {

	///
	/// Properties
	///

	@Embedded
	private EnhancedCommentDetails enhancedCommentDetails;

	private Boolean important;

	private Boolean readOnly;

	private LocalDateTime startDate;

	///
	/// Getters
	///

	public Boolean isImportant() {
		return important;
	}

	public Boolean isReadOnly() {
		return readOnly;
	}

	public Boolean getImportant() {
		return important;
	}

	@Override
	public EnhancedCommentDetails getCommentDetails() {
		return this.enhancedCommentDetails;
	}

	///
	/// Setters
	///

	public void setImportant(Boolean important) {
		this.important = important;
	}

	public void setReadOnly(Boolean readOnly) {
		this.readOnly = readOnly;
	}

	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	@Override
	public void setCommentDetails(final EnhancedCommentDetails commentDetails) {
		this.enhancedCommentDetails = commentDetails;
	}

	///
	/// Public methods
	///

	/*
	 * addComment and removeComment are used to pass (or delete respectively) the foreign key value at the child table (comments).
	 * This is a trick as there is no JPA-oriented approach to accomplish this.
	 */

	public void addComment(EnhancedComment comment) {
		if (this.enhancedCommentDetails == null) {
			final EnhancedCommentDetails enhancedCommentDetails = new EnhancedCommentDetails();
			enhancedCommentDetails.setEndDate(LocalDateTime.now().plusDays(10));
			enhancedCommentDetails.setLastUpdated(LocalDateTime.now().plusDays(1));
			this.enhancedCommentDetails = enhancedCommentDetails;
		}
		this.enhancedCommentDetails.comments.add(comment);
		comment.setPost(this);
	}

	public void removeComment(EnhancedComment comment) {
		if (this.enhancedCommentDetails == null) {
			throw new IllegalArgumentException("No such comment.");
		}
		this.enhancedCommentDetails.comments.remove(comment);
		comment.setPost(null);
	}
}