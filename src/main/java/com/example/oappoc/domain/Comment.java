package com.example.oappoc.domain;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "comments")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
		name = "solution_id",
		discriminatorType = DiscriminatorType.INTEGER,
		columnDefinition = "SMALLINT")
public abstract class Comment<P extends Post> extends EntityBase<Long> {

	///
	/// Properties
	///

	private String text;

	@ManyToOne(targetEntity = Post.class, fetch = FetchType.EAGER)
	@JoinColumn(name = "post_id")
	private P post;

	///
	/// Getters
	///

	public String getText() {
		return text;
	}

	public P getPost() {
		return post;
	}

	///
	/// Setters
	///

	public void setText(String text) {
		this.text = text;
	}

	public void setPost(final P post) {
		this.post = post;
	}
}