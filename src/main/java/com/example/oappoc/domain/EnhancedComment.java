package com.example.oappoc.domain;

import java.time.LocalDateTime;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("2")
public class EnhancedComment extends Comment<EnhancedPost> {

	///
	/// Properties
	///

	private LocalDateTime createdAt;

	///
	/// Getters
	///

	public LocalDateTime getCreatedAt() {
		return createdAt;
	}

	///
	/// Setters
	///

	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
}