package com.example.oappoc.domain;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.FetchType;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;

@MappedSuperclass
abstract class CommentDetails<C extends Comment/*, P extends Post*/> {

	///
	/// Properties
	///

	@OneToMany(
			targetEntity = Comment.class,
			mappedBy = "post",
			cascade = CascadeType.ALL,
			orphanRemoval = true,
			fetch = FetchType.LAZY)
	List<C> comments = new ArrayList<>();

	LocalDateTime endDate;

//	@OneToOne(targetEntity = Post.class, optional = true, mappedBy = "commentDetails", fetch = FetchType.LAZY)
//	P post;

	///
	/// Getters
	///

	public List<C> getComments() {
		return comments;
	}

	public LocalDateTime getEndDate() {
		return endDate;
	}

//	public P getPost() {
//		return post;
//	}

	///
	/// Setters
	///

	public void setComments(List<C> comments) {
		this.comments = comments;
	}

	public void setEndDate(LocalDateTime endDate) {
		this.endDate = endDate;
	}

//	public void setPost(final P post) {
//		this.post = post;
//	}
}