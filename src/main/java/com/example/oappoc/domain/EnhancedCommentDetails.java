package com.example.oappoc.domain;

import java.time.LocalDateTime;

import javax.persistence.Embeddable;

@Embeddable
public class EnhancedCommentDetails extends CommentDetails<EnhancedComment/* EnhancedPost*/> {

	///
	/// Properties
	///

	private LocalDateTime lastUpdated;

	///
	/// Getters
	///

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	///
	/// Setters
	///

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}
}