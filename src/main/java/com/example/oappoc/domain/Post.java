package com.example.oappoc.domain;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.Table;

@Entity
@Table(name = "posts")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(
		name = "solution_id",
		discriminatorType = DiscriminatorType.INTEGER,
		columnDefinition = "SMALLINT")
public abstract class Post<CD extends CommentDetails> extends EntityBase<Long> {

	///
	/// Properties
	///

	private String title;

	///
	/// Getters
	///

	public String getTitle() {
		return title;
	}

	public abstract CD getCommentDetails();

	///
	/// Setters
	///

	public void setTitle(final String title) {
		this.title = title;
	}

	public abstract void setCommentDetails(CD commentDetails);
}