package com.example.oappoc.domain;

import java.io.Serializable;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

@MappedSuperclass
abstract class EntityBase<ID extends Serializable> {

	///
	/// Properties
	///

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private ID id;

	///
	/// Getters
	///

	public ID getId() {
		return id;
	}

	///
	/// Setters
	///

	public void setId(ID id) {
		this.id = id;
	}
}