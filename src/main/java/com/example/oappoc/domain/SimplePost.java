package com.example.oappoc.domain;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Embedded;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue("1")
public class SimplePost extends Post<SimpleCommentDetails> {

	///
	/// Properties
	///

	@Embedded
	private SimpleCommentDetails simpleCommentDetails;

	///
	/// Getters
	///

	@Override
	public SimpleCommentDetails getCommentDetails() {
		return this.simpleCommentDetails;
	}

	///
	/// Setters
	///

	@Override
	public void setCommentDetails(final SimpleCommentDetails commentDetails) {
		this.simpleCommentDetails = commentDetails;
	}

	///
	/// Public methods
	///

	/*
	 * addComment and removeComment are used to pass (or delete respectively) the foreign key value at the child table (comments).
	 * This is a trick as there is no JPA-oriented approach to accomplish this.
	 */

	public void addComment(SimpleComment comment) {
		if (this.simpleCommentDetails == null) {
			this.simpleCommentDetails = new SimpleCommentDetails();
		}
		this.simpleCommentDetails.comments.add(comment);
		comment.setPost(this);
	}

	public void removeComment(SimpleComment comment) {
		if (this.simpleCommentDetails == null) {
			throw new IllegalArgumentException("No such comment.");
		}
		this.simpleCommentDetails.comments.remove(comment);
		comment.setPost(null);
	}
}