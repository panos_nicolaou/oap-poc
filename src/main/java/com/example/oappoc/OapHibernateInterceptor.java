package com.example.oappoc;

import java.io.Serializable;
import java.util.Iterator;

import org.hibernate.EmptyInterceptor;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component("hibernateStatisticsInterceptor")
public class OapHibernateInterceptor extends EmptyInterceptor {

	private static final long serialVersionUID = 5278832720227796822L;

	/** The logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(OapHibernateInterceptor.class);

	private ThreadLocal<Long> queryCount = new ThreadLocal<>();

	public void startCounter() {
		queryCount.set(0L);
	}

	public Long getQueryCount() {
		return queryCount.get();
	}

	public void clearCounter() {
		queryCount.remove();
	}

	@Override
	public boolean onSave(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) {
		return super.onSave(entity, id, state, propertyNames, types);
	}

	@Override
	public void onDelete(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) {
		super.onDelete(entity, id, state, propertyNames, types);
	}

	@Override
	public boolean onLoad(final Object entity, final Serializable id, final Object[] state, final String[] propertyNames, final Type[] types) {
		return super.onLoad(entity, id, state, propertyNames, types);
	}

	@Override public void postFlush(final Iterator entities) {
		super.postFlush(entities);
	}

	@Override public void preFlush(final Iterator entities) {
		super.preFlush(entities);
	}

	@Override
	public String onPrepareStatement(String sql) {
//		Long count = queryCount.get();
//		if (count != null) {
//			queryCount.set(count + 1);
//		}
//		LOGGER.info("{}----- Query: {}{}", "\u001B[36m", sql, "\u001B[0m");
		return super.onPrepareStatement(sql);
	}

	@Override
	public boolean onFlushDirty(final Object entity, final Serializable id, final Object[] currentState, final Object[] previousState,
			final String[] propertyNames, final Type[] types) {

		findDirtyFields(entity, currentState, previousState, propertyNames, types);

		return super.onFlushDirty(entity, id, currentState, previousState, propertyNames, types);
	}

	///
	/// Private methods
	///

	private static void findDirtyFields(final Object entity, final Object[] currentState, final Object[] previousState,
			final String[] propertyNames, final Type[] types) {

		for (int index = 0; index < types.length; index++) {
			final String className = entity.getClass().getSimpleName();
			final Type type = types[index];
			final String propertyName = propertyNames[index];
			final Object current = currentState[index];
			final Object previous = previousState[index];

			if (!type.isAssociationType() && type.isDirty(previous, current, null)) {
				LOGGER.info("{}{}{}{}{}", "\u001B[33m", className, ": ", propertyName, "\u001B[0m");
			}
		}
	}
}