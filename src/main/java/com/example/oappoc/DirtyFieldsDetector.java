package com.example.oappoc;

import java.lang.reflect.Field;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.persistence.EntityManagerFactory;

import org.hibernate.FlushMode;
import org.hibernate.HibernateException;
import org.hibernate.event.service.spi.EventListenerRegistry;
import org.hibernate.event.spi.EventType;
import org.hibernate.event.spi.PostUpdateEvent;
import org.hibernate.event.spi.PostUpdateEventListener;
import org.hibernate.internal.SessionFactoryImpl;
import org.hibernate.persister.entity.EntityPersister;
import org.hibernate.type.CompositeType;
import org.hibernate.type.Type;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import static java.util.stream.Collectors.toMap;

@Component
public class DirtyFieldsDetector implements PostUpdateEventListener {
	/** The logger. */
	private static final Logger LOGGER = LoggerFactory.getLogger(DirtyFieldsDetector.class);

	private final EntityManagerFactory entityManagerFactory;

	public DirtyFieldsDetector(final EntityManagerFactory entityManagerFactory) {
		this.entityManagerFactory = entityManagerFactory;
	}

	@PostConstruct
	private void init() {
		SessionFactoryImpl sessionFactory = entityManagerFactory.unwrap(SessionFactoryImpl.class);
		EventListenerRegistry registry = sessionFactory.getServiceRegistry().getService(EventListenerRegistry.class);
		registry.getEventListenerGroup(EventType.POST_UPDATE).appendListener(this);
	}

	@Override
	public void onPostUpdate(final PostUpdateEvent event) throws HibernateException {

		final int[] dirtyPropertyIndexes = event.getDirtyProperties();

		final Map<Integer, List<String>> dirtyPropertyByIndex = Arrays.stream(dirtyPropertyIndexes)
				.mapToObj(dirtyPropertyIndex -> {
					//
					final List<String> dirtyPropertyNames = getDirtyPropertyNames(event, dirtyPropertyIndex);

					return new AbstractMap.SimpleImmutableEntry<>(dirtyPropertyIndex, dirtyPropertyNames);
				})
				.collect(toMap(Map.Entry::getKey, Map.Entry::getValue));

		final String[] dirtyProperties = dirtyPropertyByIndex.values().stream()
				.flatMap(Collection::stream)
				.toArray(String[]::new);

//		if (entity instanceof EnhancedPost) {
		Arrays.stream(dirtyProperties).forEach(dirtyProperty ->
				event.getSession().createNativeQuery(
						"insert into dirty_properties (property_name, session_id) " +
								"values (:property_name, :session_id)")
						.setParameter("session_id", event.getSession().getSessionIdentifier())
						.setParameter("property_name", dirtyProperty)
						.setFlushMode(FlushMode.MANUAL)
						.executeUpdate());
//		}

		LOGGER.info("{}[{}][{}]{}{}", "\u001B[31m", event.getPersister().getRootEntityName(), event.getPersister().getEntityName(),
				dirtyProperties, "\u001B[0m");
	}

	@Override
	public boolean requiresPostCommitHanding(final EntityPersister persister) {
		return false;
	}

	@Override
	public boolean requiresPostCommitHandling(final EntityPersister persister) {
		return false;
	}

	private static List<String> getDirtyPropertyNames(final PostUpdateEvent event, final int dirtyPropertyIndex) {

		if (event.getPersister().getPropertyTypes()[dirtyPropertyIndex].isComponentType()) {
			final Object oldState = event.getOldState()[dirtyPropertyIndex];
			final Object currentState = event.getState()[dirtyPropertyIndex];

			final Map<String, Field> fieldByPropertyName = getFieldsAndGroupByPropertyName(oldState.getClass());

			final List<String> dirtyPropertyNames = new ArrayList<>();

			final CompositeType componentType = ((CompositeType) event.getPersister().getPropertyTypes()[dirtyPropertyIndex]);

			final Type[] subtypes = componentType.getSubtypes();

			for (int j = 0; j < subtypes.length; j++) {
				final Type type = subtypes[j];
				if (!type.isAssociationType()) {
					final String propertyName = componentType.getPropertyNames()[j];

					try {
						final Field field = fieldByPropertyName.get(propertyName);
						final Object oldValue = field.get(oldState);
						final Object currentValue = field.get(currentState);

						if (type.isDirty(oldValue, currentValue, event.getSession())) {
							dirtyPropertyNames.add(propertyName);
						}
					} catch (final IllegalAccessException e) {
						e.printStackTrace();
					}
				}
			}
			return dirtyPropertyNames;
		} else {
			return Collections.singletonList(event.getPersister().getPropertyNames()[dirtyPropertyIndex]);
		}
	}

	private static Map<String, Field> getFieldsAndGroupByPropertyName(final Class clazz) {

		final Map<String, Field> fieldByPropertyName = new HashMap<>();

		for (Class<?> c = clazz; c != null; c = c.getSuperclass()) {
			Arrays.stream(c.getDeclaredFields()).forEach(declaredField ->
					fieldByPropertyName.put(declaredField.getName(), declaredField));
		}

		return fieldByPropertyName;
	}
}