package com.example.oappoc.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.oappoc.domain.Post;

public interface PostRepository<P extends Post> extends JpaRepository<P, Long> {
}