package com.example.oappoc.repository;

import com.example.oappoc.domain.SimplePost;

public interface SimplePostRepository extends PostRepository<SimplePost> {
}