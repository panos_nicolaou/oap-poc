package com.example.oappoc.repository;

import com.example.oappoc.domain.EnhancedPost;

public interface EnhancedPostRepository extends PostRepository<EnhancedPost> {
}