package com.example.oappoc;

import java.util.Map;

import org.springframework.boot.autoconfigure.orm.jpa.HibernatePropertiesCustomizer;
import org.springframework.stereotype.Component;

@Component
public class OapHibernatePropertiesCustomizer implements HibernatePropertiesCustomizer {

	private final OapHibernateInterceptor oapHibernateInterceptor;

	public OapHibernatePropertiesCustomizer(final OapHibernateInterceptor oapHibernateInterceptor) {
		this.oapHibernateInterceptor = oapHibernateInterceptor;
	}

	@Override
	public void customize(final Map<String, Object> hibernateProperties) {
		hibernateProperties.put("hibernate.session_factory.interceptor", oapHibernateInterceptor);
	}
}